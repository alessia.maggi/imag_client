from setuptools import setup, find_packages

setup(name="imag_client", packages=find_packages(), install_requires=['pytest', 'obspy', 'pandas'])
