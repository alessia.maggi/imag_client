import shutil
import pytest
from pathlib import Path

from imag_client import ftp_intermagnet, iaga2002
from imag_client.iaga2002 import IagaData

TESTDATA = Path(__file__).absolute().parent.joinpath('TestData')

fname2 = TESTDATA.joinpath('clf20121114qsec.sec').as_posix()
fname3 = TESTDATA.joinpath('ams20010101dmin.min').as_posix()


@pytest.fixture()
def populated_sds_root():
    temp_sds_root = TESTDATA.joinpath('TMP')
    IagaData(TESTDATA.joinpath('ams20010101dmin.min').as_posix()).write_to_sds(temp_sds_root)
    IagaData(TESTDATA.joinpath('clf20121113qsec.sec.gz').as_posix()).write_to_sds(temp_sds_root)
    yield temp_sds_root.as_posix()
    shutil.rmtree(temp_sds_root)


@pytest.fixture()
def mock_ftp_client(monkeypatch):
    from ftplib import FTP

    def mock_pass(*args, **kwargs):
        _, _ = args, kwargs
        pass

    def mock_retrbinary(*args, **kwargs):
        _, cmd, callback_write = args
        _ = kwargs
        fname = cmd.split()[-1]

        with open(TESTDATA.joinpath(fname), 'rb') as f:
            raw_data = f.read()

        callback_write(raw_data)

    def mock_dir(*args, **kwargs):
        _, _, callback = args
        _ = kwargs
        with open(TESTDATA.joinpath('ftp_dir_return.txt')) as myfile:
            for line in myfile:
                callback(line)

    monkeypatch.setattr(FTP, "connect", mock_pass)
    monkeypatch.setattr(FTP, "login", mock_pass)
    monkeypatch.setattr(FTP, "cwd", mock_pass)
    monkeypatch.setattr(FTP, "retrbinary", mock_retrbinary)
    monkeypatch.setattr(FTP, 'dir', mock_dir)


@pytest.fixture()
def im_req_gen():
    return ftp_intermagnet.InterMagRequestGenerator()


@pytest.fixture()
def im_ftp_client(populated_sds_root):
    return ftp_intermagnet.InterMagFTPClient(populated_sds_root)


@pytest.fixture()
def magdata2():
    return iaga2002.IagaData(fname2)


@pytest.fixture()
def magdata3():
    return iaga2002.IagaData(fname3)
