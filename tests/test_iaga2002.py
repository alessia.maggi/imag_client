import obspy
import pytest
from pathlib import Path

import imag_client.iaga2002
from imag_client import iaga2002

TESTDATA = Path(__file__).absolute().parent.joinpath('TestData')


fname_empty = TESTDATA.joinpath('cyg20180204pmin.min')


@pytest.fixture()
def mock_html_response(monkeypatch):
    import pandas as pd
    import pickle

    def mock_read_html(*args, **kwargs):
        _, _ = args, kwargs
        with open(TESTDATA.joinpath('intermag_pd.dump'), 'rb') as f:
            df = pickle.load(f)
        return df

    monkeypatch.setattr(pd, "read_html", mock_read_html)


@pytest.fixture()
def im_client(mock_html_response):
    import os
    yield iaga2002.InterMagStations()
    os.remove('mag_stations_local_copy.csv')


def test_intermag_locid():
    fname = 'clf20121114qsec.sec'
    locid = iaga2002.get_locid_from_iaga2002_fname(fname)
    assert locid == 'Q'


def test_iaga2002_emptyfile():
    try:
        iaga2002.IagaData(fname_empty)
    except UnboundLocalError:
        print(f'Empty file {fname_empty}')


def test_iaga2002_read_to_pandas(magdata2):

    assert magdata2._head_numlines == 22 - 1


def test_header_to_dict(magdata2):

    stats = magdata2._stats
    print(stats)
    assert(stats['network'] == iaga2002.IagaData.NETWORK_CODE)
    assert(stats['station'] == 'CLF')
    assert(stats['stla']) == pytest.approx(48.025)


def test_iaga2002_read_to_obspy(magdata2):
    st = magdata2.stream

    assert st[0].stats.starttime == obspy.UTCDateTime(2012, 11, 14)
    assert len(st) == 4
    assert st[0].data[0] == 21151.49
    assert st[0].stats.channel == 'SX'


def test_iaga2002_read_to_obspy_def(magdata3):
    st = magdata3.stream

    assert st[0].stats['location'] == 'D'


def test_sds_fname_from_trace(magdata2):
    tr = magdata2.stream[0]

    sds_fname = imag_client.iaga2002.get_sds_fname_from_trace(tr)
    assert sds_fname == "2012/MA/CLF/SX.D/MA.CLF.Q.SX.D.2012.319"


def test_sds_writing(magdata2, populated_sds_root):
    imag_client.iaga2002.write_stream_to_sds(Path(populated_sds_root), magdata2.stream)


def test_im_client_getstations_invalid_fname(im_client):

    assert len(im_client.as_dict) == 152
    assert im_client.as_dict['AAE'].name == 'AAE'
    assert pytest.approx(im_client.as_dict['AAE'].lat, 90 - 80.97)


def test_im_client_getstations_valid_empty_fname(mock_html_response):
    import tempfile

    with tempfile.NamedTemporaryFile() as myfile:
        fname = myfile.name
        im_client = iaga2002.InterMagStations(fname)
    assert len(im_client.as_dict) == 152
    assert im_client.as_dict['AAE'].name == 'AAE'
    assert im_client.as_dict['AAE'].lat == 90 - 80.97


def test_local_file_timestamp(populated_sds_root):
    from pathlib import Path
    import datetime
    files = Path(populated_sds_root).glob('**/*.D.*')
    for file in files:
        assert imag_client.iaga2002.get_timestamp_for_local_file(file) < datetime.datetime.now()


def test_gzipped_file():
    assert imag_client.iaga2002.is_gzipped(TESTDATA.joinpath('unzip_test_file.dat.gz')) is True
    assert imag_client.iaga2002.is_gzipped(TESTDATA.joinpath('clf2012dhor.hor')) is False


def test_unzip():
    import shutil
    import os
    fname = TESTDATA.joinpath('unzip_test_file.dat.gz')
    fname_copy = f'{fname}_copy'
    shutil.copy(fname, fname_copy)
    imag_client.iaga2002.unzip_inplace(fname_copy)
    os.remove(fname_copy)


def test_intermagnet_retrieval(mock_ftp_client, im_ftp_client, im_req_gen):

    req = im_req_gen.generate_request(year=2001, month=1, sampling='minute', d_type='definitive')
    im_ftp_client.download_files_to_sds(
        full_path=req.ftp_directory,
        fnames=['ams20010101dmin.min']
    )


def test_intermagnet_retrieval_empty_file(mock_ftp_client, monkeypatch, im_ftp_client, im_req_gen):
    from ftplib import FTP

    def mock_retrbinary_empty(*args):
        _, _, callback_write = args

        with open(TESTDATA.joinpath('cyg20180204pmin.min'), 'rb') as f:
            raw_data = f.read()

        callback_write(raw_data)

    monkeypatch.setattr(FTP, "retrbinary", mock_retrbinary_empty)

    req = im_req_gen.generate_request(year=2018, month=2, sampling='minute', d_type='provisional')
    fnames = ['cyg20180204pmin.min']
    im_ftp_client.download_files_to_sds(
        full_path=req.ftp_directory,
        fnames=fnames
    )


def test_use_pandas_fwf_to_read_iaga_data():
    fname = TESTDATA.joinpath('shu20180821vsec.sec')
    magdata = iaga2002.IagaData(fname)
    traces = magdata.stream.select(channel='SD')
    pytest.approx(min(traces[0].data), -176404.66)


def test_write_to_sds(populated_sds_root):
    fname = TESTDATA.joinpath('ams20010101dmin.min')
    magdata = iaga2002.IagaData(fname)
    magdata.write_to_sds(sds_root=populated_sds_root)


def test_get_all_years(populated_sds_root):
    client = iaga2002.MagSDSClient(populated_sds_root)
    assert sorted(client.get_all_years()) == ['2001', '2012']


def test_fcc_zerodivision(populated_sds_root):
    with pytest.raises(ValueError):
        magdata = iaga2002.IagaData(TESTDATA.joinpath('fcc20110803vsec.sec.gz'))
        magdata.stream.write(Path(populated_sds_root).joinpath('test_seed.mseed'))


def test_sds_fnames_iaga_trace(populated_sds_root):
    fname = TESTDATA.joinpath('ams20010101dmin.min')
    magdata = iaga2002.IagaData(fname)
    magdata.write_to_sds(sds_root=populated_sds_root)

    client = iaga2002.MagSDSClient(sds_root=populated_sds_root)
    fname_from_iaga = client._fname_for_iaga2002_fname(fname)
    st = obspy.read(fname_from_iaga)
    assert st[0].stats.channel == 'MF'
    assert st[0].stats.network == iaga2002.IagaData.NETWORK_CODE

    fname_from_stream = imag_client.iaga2002.get_sds_fname_from_trace(st[0])
    assert TESTDATA.joinpath(fname_from_stream).name == Path(fname_from_iaga).name
