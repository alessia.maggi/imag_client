import pytest
from pathlib import Path

import imag_client.iaga2002
from imag_client import ftp_intermagnet, iaga2002

TESTDATA = Path(__file__).absolute().parent.joinpath('TestData')


def test_intermag_dirnames(im_req_gen):
    fname = 'clf20121114qsec.sec'
    dirname = im_req_gen.req_from_iaga2002(fname=fname).ftp_directory

    assert dirname == 'intermagnet/second/quasi-definitive/IAGA2002/2012/11'


def test_intermagnet_ftp_get_filenames(mock_ftp_client, im_ftp_client):

    req = im_ftp_client.req_gen.generate_request(year=2001, month=1, sampling='minute', d_type='variation')
    fnames = im_ftp_client.get_filenames_on_ftp_site(req=req)
    assert len(fnames) == 4


def test_intermagnet_ftp_get_filenames_with_datestamps(mock_ftp_client, im_ftp_client):
    import datetime
    req = im_ftp_client.req_gen.generate_request(year=2001, month=1, sampling='minute', d_type='variation')
    file_list = im_ftp_client.get_ftp_filenames_and_datestamps(req=req)
    for _, timestamp in file_list:
        assert timestamp < datetime.datetime.now()


def test_intermagnet_ftp_get_filenames_error_perm(mock_ftp_client, monkeypatch, im_ftp_client):
    from ftplib import FTP

    monkeypatch.setattr(FTP, "cwd", mock_raise_ftplib_error_perm)

    req = im_ftp_client.req_gen.generate_request(year=2001, month=1, sampling='minute', d_type='variation')
    with pytest.raises(ResourceWarning):
        _ = im_ftp_client.get_filenames_on_ftp_site(req=req)


def test_intermagnet_retrieval_error_on_login(mock_ftp_client, monkeypatch, im_ftp_client, im_req_gen):
    from ftplib import FTP
    from socket import gaierror

    req = im_req_gen.generate_request(year=2018, month=1, sampling='second', d_type='variation')
    fnames = ['drv20180115vsec.sec']

    monkeypatch.setattr(FTP, "login", mock_raise_ftplib_error_reply, im_ftp_client)
    with pytest.raises(ConnectionRefusedError):
        im_ftp_client.download_files_to_sds(
            full_path=req.ftp_directory,
            fnames=fnames
        )

    monkeypatch.setattr(FTP, "connect", mock_raise_ftplib_socket_gaierror)
    with pytest.raises(gaierror):
        im_ftp_client.download_files_to_sds(
            full_path=req.ftp_directory,
            fnames=fnames
        )


def test_request_generator(im_req_gen):
    nreq = 0
    for req in im_req_gen.requests_per_year_norestart(2001):
        if int(req.month) > 1:
            break
        nreq += 1
    assert nreq == 8


def test_request_generator_with_restart(im_req_gen):
    req_list_full = []
    for req in im_req_gen.requests_per_year(year=2001):
        req_list_full.append(req)
        if len(req_list_full) == 5:
            break

    req_list_part = req_list_full[0:2]
    restart_req = req_list_part[-1]

    for req in im_req_gen.requests_per_year(year=2001, restart_after=restart_req):
        req_list_part.append(req)
        if len(req_list_part) == 5:
            break

    assert req_list_part == req_list_full


def test_ftp_missing_iaga2002_directory(mock_ftp_client, monkeypatch, im_ftp_client):
    from ftplib import FTP

    # overmock FTP.cwd to return error_perm as FTP does
    monkeypatch.setattr(FTP, "cwd", mock_raise_ftplib_error_perm)

    year = 2001

    last_req = im_ftp_client.req_gen.generate_request(year=2001, month=1, sampling='second', d_type='quasi-definitive')

    with pytest.raises(ResourceWarning):
        for req in im_ftp_client.req_gen.requests_per_year(year=year, restart_after=last_req):
            print(req)
            fnames = im_ftp_client.get_filenames_on_ftp_site(req=req)
            im_ftp_client.download_files_to_sds(
                full_path=req.ftp_directory,
                fnames=fnames,
            )
            break


def test_ftp_download_file(mock_ftp_client, im_ftp_client):  # remove the mock client to test the real FTP site
    fname = 'drv20100224vsec.sec.gz'

    im_ftp_client.download_single_file_to_sds(fname=fname)


def test_raise_socket(mock_ftp_client, monkeypatch, im_ftp_client):
    import ftplib
    monkeypatch.setattr(ftplib.FTP, "connect", mock_raise_ftplib_socket_gaierror)
    req = im_ftp_client.req_gen.generate_request(year=2001, month=1, sampling='minute', d_type='variation')
    with pytest.raises(ConnectionAbortedError):
        try:
            _ = im_ftp_client.get_filenames_on_ftp_site(req=req)
        except ftp_intermagnet.FTP_CONNECTION_ERRORS:
            raise ConnectionAbortedError


def mock_raise_ftplib_error_perm(*args, **kwargs):
    _, _ = args, kwargs
    from ftplib import error_perm
    raise error_perm('TEST: error_perm')


def mock_raise_ftplib_error_proto(*args, **kwargs):
    _, _ = args, kwargs
    from ftplib import error_proto
    raise error_proto('TEST: error_proto')


def mock_raise_ftplib_error_reply(*args, **kwargs):
    _, _ = args, kwargs
    from ftplib import error_reply
    raise error_reply('TEST: error_reply')


def mock_raise_ftplib_socket_gaierror(*args, **kwargs):
    _, _ = args, kwargs
    import socket
    # This is raised when ftplib.FTP.connect() cannot reach the ftp site
    raise socket.gaierror(8, 'TEST: scocket.gaierror')


def test_timestamp_comparison(populated_sds_root, mock_ftp_client, im_ftp_client):
    from pathlib import Path
    req = im_ftp_client.req_gen.generate_request(year=2001, month=1, sampling='minute', d_type='variation')
    file_list = im_ftp_client.get_ftp_filenames_and_datestamps(req=req)
    sds_files = Path(populated_sds_root).glob('**/*.D.*')
    for _, timestamp in file_list:
        for sds_file in sds_files:
            assert timestamp < imag_client.iaga2002.get_timestamp_for_local_file(sds_file)


def test_extract_last_finished_from_logfile(im_req_gen):
    expected = im_req_gen.generate_request(year=2009, month=12, sampling='second', d_type='variation')
    result = ftp_intermagnet.extract_last_finished_request(TESTDATA.joinpath('2009_01.log'))

    assert expected == result


def test_extract_iaganames_from_logfile():
    expected_list = list((
        'tuc20090127dmin.min.gz',
        'asp20090107psec.sec.gz',
        'cnb20090127psec.sec.gz',
        'asp20090120psec.sec.gz',
        'lrm20090112psec.sec.gz',
        'lrm20090125psec.sec.gz',
        'aaa20090223vmin.min',
        'gna20090304psec.sec.gz',
        'lrm20090319psec.sec.gz',
        'hon20090416dmin.min.gz',
        'spt20090713vmin.min',
        'ups20090713vmin.min',
        'dou20090725dmin.min.gz',
        'cta20090712psec.sec.gz',
        ))
    fnames_for_retry = ftp_intermagnet.extract_iaga_filenames_from_file(TESTDATA.joinpath('2009_01.log'))
    assert expected_list == fnames_for_retry


def test_has_mag_data(mock_ftp_client, im_ftp_client, populated_sds_root):
    sds_root = populated_sds_root
    fname = 'ams20010101dmin.min'
    im_ftp_client.download_single_file_to_sds(fname=fname)
    client = iaga2002.MagSDSClient(sds_root=sds_root)
    has_data = client.has_data_for_iaga2002_fname(fname)
    assert has_data is True


def test_get_timestamp(mock_ftp_client, im_ftp_client, populated_sds_root):
    import datetime
    sds_root = populated_sds_root
    fname = 'ams20010101dmin.min'
    im_ftp_client.download_single_file_to_sds(fname=fname)
    client = iaga2002.MagSDSClient(sds_root=sds_root)
    timestamp = client.get_sds_timestamp_for_iaga2002_fname(fname)
    assert timestamp < datetime.datetime.now()
