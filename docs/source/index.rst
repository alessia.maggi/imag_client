.. Imag Client documentation master file, created by
   sphinx-quickstart on Tue Dec 10 11:20:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Imag Client's documentation!
=======================================

.. mdinclude:: ../../README.md



Table of contents
=================

.. toctree::
   :maxdepth: 2

   usage/installation
   usage/quickstart
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
