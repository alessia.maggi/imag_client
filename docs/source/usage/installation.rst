============
Installation
============

Here you will find instructions on how to install the package.

I'm hoping to provide both a pip and a conda package.

Using pip
---------

Blank for now

Using conda
-----------

Blank for now

Cloning and installing from git repository
------------------------------------------

Find the code on gitlab

.. code-block:: shell

   $ git clone bla
   $ python setup.py install

