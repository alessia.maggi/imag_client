from pathlib import Path, PurePath
from collections import namedtuple
from typing import Tuple, Union
from math import isclose

from obspy import UTCDateTime, Trace, Stream
from obspy.clients.filesystem.sds import Client
import pandas as pd

import warnings
import logging

Station = namedtuple('Station', ['name', 'lat', 'lon'])


# ######################
# IAGA Datafile stuff
# ######################

def get_iaga2002_datastart_linenum(fname: Union[str, Path]) -> int:
    """Get line-number of a IAGA2002 formatted file at which the data portion starts.

    Uses the end-of-line characters '|' to distinguish the header
    from the data.

    :param fname: Name of the IAGA2002 file to be read.
    :return: Line number at which data portion starts.

    Shortened version of the first few lines of ams20010101dmin.min from Intermagnet::

        Format                 IAGA-2002                                    |
        Source of Data         EOST                                         |
        Station Name           Martin de Vivies-Amsterdam Island            |
        IAGA CODE              AMS                                          |
        Geodetic Latitude      -37.796                                      |
        Geodetic Longitude     77.574                                       |
        Elevation              50                                           |
        Reported               XYZF                                         |
        Sensor Orientation     HDZF                                         |
        Digital Sampling       2                                            |
        Data Interval Type     Average 1-Minute (00:30-01:29)               |
        Data Type              Definitive                                   |
        # D-conversion factor                                               |
        # K9-limit             460                                          |
        # This data file was converted from INTERMAGNET CD-ROM              |
        # Format binary data.                                               |
        DATE       TIME         DOY     AMSX      AMSY      AMSZ      AMSF  |
        2001-01-01 00:00:00.000 001     13863.40 -12354.30 -48532.20  51963.60
        2001-01-01 00:01:00.000 001     13863.40 -12354.30 -48532.20  51963.70
        2001-01-01 00:02:00.000 001     13863.50 -12354.50 -48532.20  51963.70
        2001-01-01 00:03:00.000 001     13863.60 -12354.50 -48532.10  51963.70

    Example::

        >>> from iaga2002 import get_iaga2002_datastart_linenum
        >>> get_iaga2002_datastart_linenum('ams20010101dmin.min')
        16

    """
    lookup = '|'

    with open(fname) as myFile:
        for num, line in enumerate(myFile):
            if lookup not in line:
                break
    return num - 1


def split_iaga2002_fname(fname: Union[str, Path]) -> Tuple[str, str, str, str, str, str]:
    """Split the name of an Intermagnet data file into its components.

    :param fname: Name of the IAGA2002 file.
    :return: Station-name, year, month, day, data-quality ['variation'|
        'provisional'|'quasi-definitive'|'definitive'], sampling
        ['minute'|'second'].

    Filenames of IAGA2002 formatted files on the Intermagnet FTP site contain
    information on their contents. Sometimes this information is not repeated
    in the headers of the files so needs to be extracted from their names.

    Data quality string translations::

        'v' -> 'variation'
        'p' -> 'provisional'
        'q' -> 'quasi-definitive'
        'd' -> 'definitive'

    Sampling string translations::

        'hor' -> 'hour'
        'min' -> 'minute'
        'ssec' -> 'second'

    Example::

        >>> from iaga2002 import split_iaga2002_fname
        >>> split_iaga2002_fname('any/path/ams20010101dmin.min')
        ('ams', '2001', '01', '01', 'definitive', 'minute')
    """

    path = Path(fname)
    name = path.name

    iaga_dict = {
        'v': 'variation',
        'p': 'provisional',
        'q': 'quasi-definitive',
        'd': 'definitive',
        'min': 'minute',
        'sec': 'second',
        'hor': 'hour',
    }

    staname = name[0:3]
    year = name[3:7]
    month = name[7:9]
    day = name[9:11]
    type_string = iaga_dict[name[11:12]]
    samp_string = iaga_dict[name[12:15]]

    return staname, year, month, day, type_string, samp_string


def get_locid_from_iaga2002_fname(fname: Union[str, Path]) -> str:
    """Extract miniSEED-compatible location codes from Intermagnet filenames.

    Calls
    :func:`split_iaga2002_fname` to parse the filename.

    :param fname: Name of the IAGA2002 file.
    :returns: The location code.

     The location code depends directly on the data-quality::

        'variation' -> 'V'
        'provisional' -> 'P'
        'quasi-definitive' -> 'Q'
        'definitive' -> 'D'

    Example::

        >>>from iaga2002 import get_locid_from_iaga2002_fname
        >>>get_locid_from_iaga2002_fname('any/path/ams20010101dmin.min')
        'D'
    """
    _, _, _, _, type_string, _ = split_iaga2002_fname(fname)

    locid_dict = {
        'variation': 'V',
        'provisional': 'P',
        'quasi-definitive': 'Q',
        'definitive': 'D',
    }

    return locid_dict[type_string]


def get_sampling_code_from_iaga2002_fname(fname):
    """Extract miniSEED-compatible sampling-rate codes from Intermagnet filenames.

    Calls :func:`split_iaga2002_fname` to parse the filename.

    :param fname: Name of the IAGA2002 file.
    :returns: The sampling-rate code.

    Possible sampling rate codes::

        'hour' -> 'H'
        'minute' -> 'M'
        'second' -> 'S'

    Example::

        >>>from iaga2002 import get_sampling_code_from_iaga2002_fname
        >>>get_sampling_code_from_iaga2002_fname('any/path/ams20010101dmin.min')
        'M'
    """
    _, _, _, _, _, samp_string = split_iaga2002_fname(fname)

    samp_dict = {
        'minute': 'M',
        'second': 'S',
        'hour': 'H',
    }

    return samp_dict[samp_string]


def get_sampling_code_from_delta(delta: float) -> str:
    """Convert sampling intervals to miniSEED-compatible sample rate codes.

    Used to derive the sampling code from the data themselves.

    :param delta: Sampling interval in seconds.
    :returns: The sampling-rate code.

    Possible sampling rate codes::

        delta = 3600 -> 'H' (hour)
        delta = 60 -> 'M' (minute)
        delta = 1 -> 'S' (second)
        other values -> 'U' (undetermined)

    Example::

        >>>from iaga2002 import get_sampling_code_from_delta
        >>>get_sampling_code_from_delta(1)
        'S'

    .. note::
        Floating-point comparisons are made with :func:`isclose`, which allows a tolerance
        to account for rounding errors::

            >>>get_sampling_code_from_delta(60.0000001)
            'U'
            >>>get_sampling_code_from_delta(60.0000001)
            'M'
    """

    keywords_samp = [1, 60, 360]
    values_samp = ['S', 'M', 'H']

    dict_samp = dict(zip(keywords_samp, values_samp))

    loc_samp = 'U'
    for key, value in dict_samp.items():
        if isclose(delta, key):
            loc_samp = value
            break

    return loc_samp


class IagaData:
    """Class that transforms Intermagnet data from IAGA2002 format to an Obspy :class:`Stream` object.

    Once transformed, you can apply the same filters, spectrograms and other analysis tools
    as are traditionally applied in seismology thanks to the :mod:`obspy` package.

    :param fname: Name of the IAGA2002 file.
    :param iaga_fname: If given, is used instead of `_fname` to derive the data-quality
        and sampling codes.

    :ivar stream: Obspy :class:`Stream` containing the data from Intermagnet data file.

    """

    NETWORK_CODE = 'MA'
    """Network code for Intermagnet data."""

    def __init__(self, fname: Union[str, Path], iaga_fname: str = None):
        self._fname = fname
        self._stats = {}
        self.stream = None
        self._unzip_if_required()
        self._head_numlines = get_iaga2002_datastart_linenum(fname)
        self._read_iaga2002_to_obspy_stream(iaga_fname=iaga_fname)

    @property
    def obspy_stream(self) -> Stream:
        """ Read-only access to the :attr:`stream` attribute.
        .. warn:: Deprecated. Use direct access to the :attr:`stream` attribute instead.
        """
        warnings.warn('Use IagaData.stream directly', DeprecationWarning)
        return self.stream

    def __str__(self):
        return self.stream.__str__()

    def write_to_sds(self, sds_root: Union[str, Path]):
        """ Write the data stream in miniseed format to an SDS file structure.

        The SDS (SeiscomP Data Structure) file structure is used by XXX to store large volumes
        of waveform data locally for rapid retrieval. One file is created for each network (NET),
        station (STA), location code (LOC), channel (CHA), year (YEAR), and Julian day (JDAY)::

            sds_root/YEAR/NET/STA/CHA.D/NET.STA.LOC.CHA.D.YEAR.JDAY

        For more information about the SDS data structure see xxxxxxxxx
        TODO: provide link to explanation of SDS structure.

        :param sds_root: Path to the root directory of the SDS structure.

        """
        if type(sds_root) is str:
            write_stream_to_sds(Path(sds_root), self.stream)
        else:
            write_stream_to_sds(sds_root, self.stream)

    def _read_iaga2002_to_pandas(self):
        warnings.warn('Call read_iaga2002_to_pandas_fwf instead.', DeprecationWarning)
        self.df = pd.read_csv(self._fname, sep=r'\s+', header=self._head_numlines)

    def _read_iaga2002_to_pandas_fwf(self):
        colspecs = [(0, 10), (11, 23), (24, 27), (30, 40), (40, 50), (50, 60), (60, 70)]
        self.df = pd.read_fwf(self._fname, colspecs=colspecs, header=self._head_numlines, compression=None)

    def _get_float_stat_safely(self, keyword, string):
        try:
            self._stats[keyword] = float(string)
        except ValueError:
            pass

    def _read_iaga2002_header_to_dict(self):

        with open(self._fname) as myFile:
            for num, line in enumerate(myFile):

                if num > self._head_numlines:
                    break

                line_lower = line.lower()
                line_strip = line[24:58].strip()
                line_lower_strip = line_lower[24:58].strip()

                if "iaga code" in line_lower:
                    self._stats['station'] = line_strip
                elif "data type" in line_lower:
                    self._stats["data_type"] = line_lower_strip
                elif "geodetic latitude" in line_lower:
                    self._get_float_stat_safely('stla', line_strip)
                elif "geodetic longitude" in line_lower:
                    self._get_float_stat_safely('stlo', line_strip)
                elif "elevation" in line_lower:
                    self._get_float_stat_safely('stel', line_strip)

        self._stats['network'] = self.NETWORK_CODE

    def _read_iaga2002_to_obspy_stream(self, iaga_fname):

        self._read_iaga2002_header_to_dict()
        self._read_iaga2002_to_pandas_fwf()

        (npts, __) = self.df.shape
        self._stats['npts'] = npts

        starttime = UTCDateTime("%s %s" % (self.df['DATE'].values[0], self.df['TIME'].values[0]))
        self._stats['starttime'] = starttime

        starttime_plus_one = UTCDateTime("%s %s" % (self.df['DATE'].values[1], self.df['TIME'].values[1]))
        if starttime_plus_one == starttime:
            raise ValueError(f'Delta is zero in file {iaga_fname}')
        self._stats['delta'] = starttime_plus_one - starttime

        if iaga_fname is not None:
            self._fname = iaga_fname
        self._stats['location'] = get_locid_from_iaga2002_fname(self._fname)

        tr_list = []
        for col in self.df.columns:
            if self._stats['station'] in col:
                mystats = self._stats.copy()

                sampling_code = get_sampling_code_from_delta(self._stats['delta'])

                mystats['channel'] = col.replace(mystats['station'], sampling_code)
                if '|' in mystats['channel']:
                    mystats['channel'] = mystats['channel'].replace('|', '').strip()
                self.df[col] = self.df[col].apply(pd.to_numeric)

                tr = Trace(data=self.df[col].values[:], header=mystats)
                tr_list.append(tr)

        self.stream = Stream(traces=tr_list)

    def _unzip_if_required(self):
        if is_gzipped(self._fname):
            try:
                unzip_inplace(self._fname)
            except EOFError as err:
                logging.warning(f'Unzipping problem with {self._fname}. Caught {err}.')
            except FileNotFoundError as err:
                logging.warning(f'Could not find {self._fname}. Caught {err}')


# ######################
# INTERMAGNET  stuff
# ######################

class InterMagStations:
    """ Handles basic meta-data for Intermagnet stations.

    Upon initialization, station names, latitudes and longitudes are either read from a local
    csv file or parsed from the Intermagnet website. If the local csv file does not exist, it
    is created and populated with the metadata from the Intermagnet website.

    For each station the metadata is stored in a :class:`Station` NamedTuple.

    :param local_filename: Name of the local csv file from/to which to read/write the station metadata.
    """

    INTERMAGNET_STATION_WEBPAGE = 'http://www.intermagnet.org/imos/imotblobs-eng.php'
    """The Intermagnet webpage that contains the station metadata."""

    def __init__(self, local_filename: str = 'mag_stations_local_copy.csv'):

        self._im_stations = dict()
        self._local_filename = local_filename
        self._populate_im_stations()

    @property
    def as_dict(self):
        """Return a dictionary of Intermagnet stations, indexed by station name."""
        return self._im_stations

    @property
    def as_list(self):
        """Return a list of Intermagnet stations sorted alphabetically by station name."""
        return sorted(self.as_dict.values())

    @property
    def as_pandas_dataframe(self):
        """Return a pandas data-frame with three columns: 'name', 'lat', 'lon'."""
        return pd.DataFrame.from_dict(self.as_dict, orient='index', columns=['name', 'lat', 'lon'])

    def _populate_im_stations(self):
        local_file_exists = Path(self._local_filename).is_file()
        if not local_file_exists:
            self._get_intermagnet_stations()
            self._write_stadict()
        else:
            try:
                self._read_stadict()
            except ValueError:
                logging.warning(f"Failed to read dict from file {self._local_filename}. Trying to get it from"
                                f"intermagnet website {self.INTERMAGNET_STATION_WEBPAGE} instead.")
                self._get_intermagnet_stations()
                self._write_stadict()

    def _read_stadict(self):
        from pandas.errors import EmptyDataError
        logging.info(f"Trying to read station list from file {self._local_filename}.\n")
        try:
            df = pd.read_csv(self._local_filename)
        except EmptyDataError:
            raise ValueError

        self._df_to_stadict(df)

    def _df_to_stadict(self, df):
        for row in zip(df['name'].values, df['lat'].values, df['lon'].values):
            sta = Station._make(row)
            self._im_stations[sta.name] = sta

    def _write_stadict(self):
        try:
            self.as_pandas_dataframe.to_csv(self._local_filename)
        except FileNotFoundError:
            logging.warning(f"Failed to write station dict to file {self._local_filename}. "
                            f"If you want a local copy of the dictionary on the filesystem, "
                            f"provide a valid name to InterMagClient.")

    def _get_intermagnet_stations(self):
        """
        Reads clean version of intermagnet station information from the intermagnet website.
        The intermagnet dataframe read directly from the intermagnet website needs cleaning to be used. Cleaning
        involves removing the * from the names of non-current stations, turning colatitudes into latitudes, and
        recasting longitudes to between -180 and 180 degrees.
        :return:
        """
        logging.info(f"Trying to get station dict from intermagnet website {self.INTERMAGNET_STATION_WEBPAGE}.\n")
        df_orig = pd.read_html(InterMagStations.INTERMAGNET_STATION_WEBPAGE)
        df = df_orig[0]
        self._cleanup_station_df(df)
        self._df_to_stadict(df)

    @staticmethod
    def _cleanup_station_df(df):
        df['name'] = [row.replace("*", "") for row in df["IAGA"]]
        colat = [row.replace("°", "") for row in df["Colatitute"]]
        lon = [row.replace("°", "") for row in df["East Longitude"]]
        df['lat'] = [90 - float(x) for x in colat]
        df['lon'] = [float(x) for x in lon]
        df['lon'] = df['lon'].apply(lambda x: x if x <= 180 else x - 360)


class MagSDSClient(Client):
    """An obspy client that specialises in Intermagnet data."""

    def __init__(self, sds_root: str):
        super().__init__(sds_root=sds_root)

    def _fname_for_iaga2002_fname(self, iaga2002fname):

        staname, year, month, day, type_string, samp_string = split_iaga2002_fname(iaga2002fname)

        network = IagaData.NETWORK_CODE
        station = staname.upper()
        location = get_locid_from_iaga2002_fname(iaga2002fname)
        samp_code = get_sampling_code_from_iaga2002_fname(iaga2002fname)
        channel = f'{samp_code}F'
        time = UTCDateTime(year=int(year), month=int(month), day=int(day))

        return super()._get_filename(network, station, location, channel, time)

    def has_data_for_iaga2002_fname(self, iaga2002fname: Union[str, Path]) -> bool:
        """Parses the IAGA2002 Intermagnet filename and checks if the SDS contains corresponding data.

        :param iaga2002fname: IAGA2002 Intermagnet filename to parse.
        """
        from os.path import exists
        filename = self._fname_for_iaga2002_fname(iaga2002fname)
        return exists(filename)

    def get_sds_timestamp_for_iaga2002_fname(self, iaga2002fname):
        if self.has_data_for_iaga2002_fname(iaga2002fname):
            sds_filename = self._fname_for_iaga2002_fname(iaga2002fname)
            timestamp = get_timestamp_for_local_file(sds_filename)
            return timestamp

    def get_all_stations(self, **kwargs):
        years = self.get_all_years()
        year_paths = [Path('/'.join([self.sds_root, year, 'MA'])) for year in years]
        sta_list = []
        for year_path in year_paths:
            sta_path_list = year_path.glob('*')
            for sta_path in sta_path_list:
                sta_list.append(sta_path.name)
        return set(sta_list)

    def get_all_years(self):
        year_list = []
        year_path_list = Path('/'.join([self.sds_root])).glob('*')
        for year_path in year_path_list:
            year_list.append(year_path.name)
        return sorted(year_list)

    def has_data_at_time(self, network, station, location, channel, time):
        from os.path import exists
        fname = super()._get_filename(network, station, location, channel, time)
        return exists(fname)


def write_stream_to_sds(root: Path, st: Stream):
    for tr in st:
        tr_name = get_sds_fname_from_trace(tr)
        tr_path = root.joinpath(tr_name)
        write_to_mseed(tr, tr_path)


def write_to_mseed(tr: Trace, path: Path):
    if not path.parent.exists():
        path.parent.mkdir(parents=True, exist_ok=True)
    tr.write(path.as_posix(), format='MSEED')


def get_sds_fname_from_trace(tr):

    jday_str = "%03d" % tr.stats.starttime.julday
    year_str = "%d" % tr.stats.starttime.year

    fname = f"{tr.stats.network}.{tr.stats.station}.{tr.stats.location}.{tr.stats.channel}.D.{year_str}.{jday_str}"

    pathname = PurePath(year_str, tr.stats.network, tr.stats.station, f"{tr.stats.channel}.D", fname)

    return pathname.as_posix()


def get_timestamp_for_local_file(filename):
    import datetime
    timestamp = datetime.datetime.fromtimestamp(Path(filename).stat().st_mtime)
    return timestamp


def is_gzipped(fname):
    import binascii

    with open(fname, 'rb') as test_f:
        return binascii.hexlify(test_f.read(2)) == b'1f8b'


def unzip_inplace(myfilename):
    import gzip
    with open(myfilename, 'rb') as myfile:
        raw_data = myfile.read()
    with open(myfilename, 'wb') as myfile:
        myfile.write(gzip.decompress(raw_data))
