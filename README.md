# ImagClient

[![Project Status: Active – The project has reached a stable, usable state and is being actively developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pipeline status](https://gitlab.com/alessia.maggi/imag_client/badges/master/pipeline.svg)](https://gitlab.com/alessia.maggi/imag_client/commits/master)
[![coverage report](https://gitlab.com/alessia.maggi/imag_client/badges/master/coverage.svg)](https://gitlab.com/alessia.maggi/imag_client/commits/master)
[![Documentation Status](https://readthedocs.org/projects/imag-client/badge/?version=latest)](https://imag-client.readthedocs.io/en/latest/?badge=latest)



A data retreival client to retreive Intermagnet data upon demand. Data are 
translated to miniseed (a standard seismology format).

## Full documentation

Full documentation available on [ReadTheDocs](https://imag-client.readthedocs.io/en/latest/).